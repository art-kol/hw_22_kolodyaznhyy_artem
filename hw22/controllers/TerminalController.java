package ru.hw.hw22.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.hw.hw22.services.Terminal;

@RestController
@RequestMapping("/terminal")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class TerminalController {

    private boolean accessToTerminal = false;

    @Autowired
    private Terminal terminal;

    @GetMapping(value = "/input")
    public String checkPinCode(@RequestParam(name = "cardNumber") String cardNumber,
                               @RequestParam(name = "pin") String pinCode) {
        String checkPinCode = terminal.checkPinCode(cardNumber, pinCode);
        accessToTerminal = terminal.accessToTerminal;
        return checkPinCode;
    }

    @GetMapping(value = "/balance")
    public String getBalance(){
        if(!accessToTerminal){
            return "Доступ запрещен";
        }
        return "Баланс: "+terminal.checkClientBalance();
    }

    @GetMapping(value = "/transfer-client")
    public String transferToClient(@RequestParam(name = "amount") Integer amount,
                          @RequestParam(name = "client") String recipient){
        if(!accessToTerminal){
            return "Доступ запрещен";
        }
        return terminal.doTransferClientToClient(amount, recipient);
    }


    @GetMapping(value = "/deposite")
    public String doDeposite(@RequestParam(name = "amount") Integer amount){
        if(!accessToTerminal){
            return "Доступ запрещен";
        }
        return terminal.doDepositIntoYourAccount(amount);
    }

    @GetMapping(value = "/finish")
    public String finish(){
        accessToTerminal = false;
        return "Приходите еще";
    }
}
