package ru.hw.hw22.entity;

public class Client {

    private String name;
    private String lastName;
    private String patronymic;
    private String cardNumber;
    private String pinCode;
    private int wallet;

    public Client(String name, String lastName, String patronymic,
                  String cardNumber, String pinCode, int wallet) {
        this.name = name;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.cardNumber = cardNumber;
        this.pinCode = pinCode;
        this.wallet = wallet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastName;
    }

    public void setLastname(String lastname) {
        this.lastName = lastname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public int getWallet() {
        return wallet;
    }

    public void setWallet(int wallet) {
        this.wallet = wallet;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", lastname='" + lastName + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", pinCode='" + pinCode + '\'' +
                ", wallet=" + wallet +
                '}';
    }
}
