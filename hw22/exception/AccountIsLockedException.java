package ru.hw.hw22.exception;

public class AccountIsLockedException extends Exception{

    public AccountIsLockedException(String message) {
        super(message);
    }

}
