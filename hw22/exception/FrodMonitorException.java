package ru.hw.hw22.exception;

public class FrodMonitorException extends Exception{

    public FrodMonitorException(String message) {
        super(message);
    }

}
