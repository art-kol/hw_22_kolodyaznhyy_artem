package ru.hw.hw22.exception;

public class PinValidationException extends Exception{

    public PinValidationException(String message) {
        super(message);
    }

}
