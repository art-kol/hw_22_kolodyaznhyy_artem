package ru.hw.hw22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hw.hw22.entity.Client;
import ru.hw.hw22.exception.FrodMonitorException;

@Service
public class FrodMonitor {

    @Autowired
    private TerminalServer terminalServer;

    public boolean checkTransfer(Client client, int amount) throws FrodMonitorException {
        if(amount%100!=0){
            throw new FrodMonitorException("Transfer amount is not a multiple of 100");
        } else if(terminalServer.getClientWallet(client)>=amount){
            return true;
        } else {
            throw new FrodMonitorException("Insufficient funds to transfer");
        }
    }

    public boolean checkDeposit(int amount) throws FrodMonitorException {
        if(amount%100!=0){
            throw new FrodMonitorException("Deposit amount is not a multiple of 100");
        } else {
            return true;
        }
    }
}
