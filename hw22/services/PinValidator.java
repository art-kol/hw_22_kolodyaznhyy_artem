package ru.hw.hw22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hw.hw22.exception.PinValidationException;

@Service
public class PinValidator {

    @Autowired
    private TerminalServer terminalServer;

    public boolean pinCodeValidation(String cardNumber, String pinCode) throws PinValidationException {
        if(terminalServer.getClientByCardnumberAndPincode(cardNumber, pinCode) == null){
            throw new PinValidationException("Incorrect pin code");
        } else {
            return true;
        }
    }
}
