package ru.hw.hw22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.hw.hw22.entity.Client;
import ru.hw.hw22.exception.AccountIsLockedException;
import ru.hw.hw22.exception.AccountNotFoundException;
import ru.hw.hw22.exception.FrodMonitorException;
import ru.hw.hw22.exception.PinValidationException;

@Service
public class Terminal {

    public boolean accessToTerminal = false;
    private Client sender;
    private int accessAttempt = 3;

    @Autowired
    private TerminalServer terminalServer;
    @Autowired
    private PinValidator pinValidator;
    @Autowired
    private FrodMonitor frodMonitor;

    public String checkPinCode(String cardNumber, String pinCode) {
        try {
            if(pinValidator.pinCodeValidation(cardNumber,pinCode)){
                accessToTerminal = true;
                sender = terminalServer.getClientByCardnumberAndPincode(cardNumber, pinCode);
                return "Доступ разрешен"+" "+"Здравствуйте "+sender.getName()+" "+cardNumber+" "+pinCode;
            }
        } catch (PinValidationException e) {
            try {
                accessAttempt++;
                if(accessAttempt == 1){
                    throw new AccountIsLockedException("Incorrect pin code. Your account has been blocked for 5 seconds");
                }
                accessAttempt--;
                return "Неправильный пин код"+" "+"Осталось попыток:"+accessAttempt--;
            } catch (AccountIsLockedException ex){
                accessAttempt = 3;
                return "Ваш аккаунт заблокирован на 5 секунд";
            }
        }
        return "Запрос не может быть обработан";
    }

    public Integer checkClientBalance(){
        return sender.getWallet();
    }

    public String doTransferClientToClient(int amount, String recipientCard) {
        try {
            Client recipient = terminalServer.getClientByCardNumber(recipientCard);
            if(recipient==null){
                throw new AccountNotFoundException("Account recepient not found");
            }
            if(frodMonitor.checkTransfer(sender, amount)){
                terminalServer.transferClientToClient(sender, recipient, amount);
                return "SUCCES: You transfer "+amount+" "+recipient.getName();
            }
        } catch (AccountNotFoundException | FrodMonitorException e) {
            return "FAILURE: "+e.getMessage()+"\n";
        }
        return "Запрос не может быть обработан";
    }


    public String doDepositIntoYourAccount(int amount){
        try {
            if(frodMonitor.checkDeposit(amount)){
                sender.setWallet(sender.getWallet()+amount);
                return "SUCCES: You deposited in your account: "+amount+"\n";
            }
        } catch (FrodMonitorException e) {
            return "FAILURE: "+e.getMessage()+"\n";
        }
        return "Запрос не может быть обработан";
    }




}
